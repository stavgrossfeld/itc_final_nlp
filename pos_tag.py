from scipy.optimize import fmin_l_bfgs_b
import pandas as pd
import numpy as np


class posTagger():

    def __init__(self, text):
        self.NUMBER_OF_FEATURES = 3
        self.text = text
        sentence_train = []
        for sentence in text:
            word_tags = [tuple((word_tag.split("_"))) for word_tag in
                         sentence.split()]
            sentence_train.append(word_tags)
        self.sentence_train = sentence_train
        self.f_100 = 0
        self.f_103 = 0
        self.f_104 = 0
        self.labels = 0
        self.probs = 0
        self.v = 0
        self.create_features(sentence_train)
        self.sentences = self.feature_extraction()
        self.f_100_inv = {v: k for k, v in self.f_100.items()}
        self.f_103_inv = {v: k for k, v in self.f_103.items()}
        self.f_104_inv = {v: k for k, v in self.f_104.items()}
        self.features = [self.f_100, self.f_103, self.f_104]
        len_v = sum([len(x.keys()) for x in self.features])
        self.linear_term = np.zeros(len_v)
        for sentence in self.sentences:
            for index, word in enumerate(sentence.index):
                for feature in range(self.NUMBER_OF_FEATURES):
                    feat_vector = np.zeros(len_v)
                    feat_vector[sentence.iloc[index, feature]] = 1
                    self.linear_term += feat_vector


    def create_features(self, text):
        f_100, f_103, f_104, labels = [], [], [], []
        f_100_dict, f_103_dict, f_104_dict = {}, {}, {}

        for i, sentence in enumerate(text):
            for index, word in enumerate(sentence):
                if word[1] not in labels:
                    labels.append(word[1])
                if word not in f_100:
                    f_100.append((word[0], word[1]))
                if index > 0:
                    bigram = (sentence[index-1][1],word[1])
                    if bigram not in f_104:
                        f_104.append(bigram)
                    if index >1:
                        trigram = (sentence[index-2][1], sentence[index-1][1],word[1])
                        if trigram not in f_103:
                            f_103.append(trigram)
        for i in np.arange(len(f_100)):
            f_100_dict[f_100[i]] = int(i) + 3
        for i in np.arange(len(f_103)):
            f_103_dict[f_103[i]] = int(i)+len(f_100)
        for i in np.arange(len(f_104)):
            f_104_dict[f_104[i]] = int(i)+len(f_103)+len(f_100)
        f_103_dict['-1'] = 0
        f_103_dict['-2'] = 1
        f_104_dict['-1'] = 2
        self.labels = labels
        self.f_100 = f_100_dict
        self.f_103 = f_103_dict
        self.f_104 = f_104_dict


    def feature_extraction(self):

        feature_df_list = []
        for index_sentence, sentence in enumerate(self.sentence_train):
            f_100_v = np.zeros(len(sentence))
            f_103_v = np.zeros(len(sentence))
            f_104_v = np.zeros(len(sentence))

            for index, element in enumerate(sentence):
                f_100_v[index] = self.f_100[element]
                if index > 1:
                    f_103_v[index] = self.f_103[(sentence[index - 2][1], sentence[index - 1][1], element[1])]
                if index == 1:
                    f_103_v[index] = 1
                if index > 0:
                    f_104_v[index] = self.f_104[(sentence[index - 1][1], element[1])]
                if index == 0:
                    f_103_v[index] = 0
                    f_104_v[index] = 2

            sentence_df = pd.DataFrame(
                np.column_stack((f_100_v, f_103_v, f_104_v)))
            sentence_df.index = [element[0] for element in sentence]
            sentence_df.columns = ["f_100", "f_103", "f_104"]

            for col in sentence_df:
                sentence_df[col] = sentence_df[col].apply(lambda val: int(val))

            feature_df_list.append(sentence_df)
        return(feature_df_list)

    def loss(self, v):
        linear_term = 0
        for sentence in self.sentences:
            for index, word in enumerate(sentence.index):
                for feature in range(self.NUMBER_OF_FEATURES):
                    linear_term += v[sentence.iloc[index,feature]]
        normal_term = 0
        for sentence in self.sentences:
            for index, word in enumerate(sentence.index):
                sum_vector = 0
                for label in self.labels:
                    for key in self.f_100.keys():
                        if key[0] == word and (word, label) in self.f_100.keys():
                            sum_vector += np.exp(v[self.f_100[(key[0], label)]])
                if index > 0:
                    for key in self.f_104.keys():
                        bigram = self.f_104_inv[sentence.iloc[index, 2]]
                        if key[0] == bigram[0] and (bigram[0], label) in self.f_104.keys():
                            sum_vector += np.exp(v[self.f_104[(key[0], label)]])
                if index > 1:
                    for key in self.f_103.keys():
                        trigram = self.f_103_inv[sentence.iloc[index, 1]]
                        if key[0] == trigram[0] and key[1] == trigram[1] and (trigram[0], trigram[1], label) in self.f_103.keys():
                            sum_vector += np.exp(v[self.f_103[(key[0], key[1], label)]])
                normal_term += np.log(sum_vector)
        print(linear_term - normal_term)
        return(- linear_term + normal_term)

    def log_linear_model(self):
        print(sum([len(x.keys()) for x in self.features]))
        self.v = fmin_l_bfgs_b(self.loss, np.zeros(
            sum([len(x.keys()) for x in self.features])),
                               fprime=self.loss_grad, disp=100)

    def loss_grad(self, v):
        len_v = len(v)
        normal_term = np.zeros(len_v)
        for sentence in self.sentences:
            for index, word in enumerate(sentence.index):
                for key in self.f_100.keys():
                    for label in self.labels:
                        if key[0] == word and (word, label) in self.f_100.keys():
                            normal_term[self.f_100[key]] += self.prob_100(sentence, index, label, word, v)
                for key in self.f_104.keys():
                    if index > 0:
                        bigram = self.f_104_inv[sentence.iloc[index, 2]]
                        for label in self.labels:
                            if key[0] == bigram[0] and (bigram[0], label) in self.f_104.keys():
                                normal_term[self.f_104[bigram]] += self.prob_104(sentence, index, (bigram[0], label), v)
                for key in self.f_103.keys():
                    if index > 1:
                        trigram = self.f_103_inv[sentence.iloc[index, 1]]
                        for label in self.labels:
                            if key[0] == trigram[0] and key[1] == trigram[1] and (trigram[0], trigram[1], label) in self.f_103.keys():
                                normal_term[self.f_103[trigram]] += self.prob_103(sentence, index, (trigram[0], trigram[1], label), v)
        print(np.mean(- self.linear_term + normal_term))
        return (- self.linear_term + normal_term)

    def viterbi(self, sentence):
        """The Viterbi algorithm for selecting tag sequences"""
        n = len(sentence)
        no_tags = len(self.labels)
        pi = np.ones((n, no_tags, no_tags))
        bp = np.zeros(pi.shape)
        for k in range(0, n):
            for u in range(0, no_tags):
                for v in range(0, no_tags):
                    maxlist = np.array([pi[k - 1, t, u] * self.prob_2(sentence, k, self.labels[v], sentence[k], self.v, [self.f_100[(sentence[k], self.labels[u])] if (sentence[k], self.labels[u]) in self.f_100.keys() else -1, self.f_103[(self.labels[u], self.labels[v], self.labels[t])] if (self.labels[u], self.labels[v], self.labels[t]) in self.f_103.keys() else -1, self.f_104[(self.labels[v], self.labels[t])] if (self.labels[v], self.labels[t]) in self.f_104.keys() else -1]) for t in range(0, no_tags)])
                    bp[k, u, v] = np.argmax(maxlist)
                    pi[k, u, v] = maxlist[int(bp[k, u, v])]
        tag_seq = np.zeros((n))
        tag_seq[n - 2], tag_seq[n - 1] = np.unravel_index(pi[-1].argmax(),
                                                          pi[-1].shape)
        for k in range(n - 3, 0):
            tag_seq[k] = bp[k + 2, tag_seq[k + 1], tag_seq[k + 2]]
        return [self.labels[int(i)] for i in tag_seq]

    def prob_100(self, sentence, word_index, y, x, v):
        linear_term = 0
        for feature in range(self.NUMBER_OF_FEATURES):
            linear_term += v[sentence.iloc[word_index, feature]]
        normal_term = 0
        for feature in self.features:
            for key in feature.keys():
                if key[1] == y and key[0] == x:
                    normal_term += np.exp(v[feature[key]])
        return np.exp((linear_term - normal_term))

    def prob_104(self, sentence, word_index, y, v):
        linear_term = 0
        for feature in range(self.NUMBER_OF_FEATURES):
            linear_term += v[sentence.iloc[word_index, feature]]
        normal_term = 0
        for feature in self.f_104.keys():
            if feature == y:
                normal_term += np.exp(v[self.f_104[feature]])
        return np.exp((linear_term - normal_term))

    def prob_103(self, sentence, word_index, y, v):
        linear_term = 0
        for feature in range(self.NUMBER_OF_FEATURES):
            linear_term += v[sentence.iloc[word_index, feature]]
        normal_term = 0
        for feature in self.f_103.keys():
            if feature == y:
                normal_term += np.exp(v[self.f_103[feature]])
        return np.exp((linear_term - normal_term))

    def prob_2(self, sentence, word_index, y, x, v, feature_vector):
        linear_term = 0
        for feature in self.features:
            for key in feature.keys():
                if key[0] == x:
                    product = np.zeros(len(self.v[0]))
                    if feature_vector[0] != -1:
                        product[feature_vector[0]] = 1
                    if feature_vector[1] != -1:
                        product[feature_vector[1]] = 1
                    if feature_vector[2] != -1:
                        product[feature_vector[2]] = 1
                    linear_term += np.dot(self.v[0], product)
        normal_term = 0
        for feature in self.features:
            for key in feature.keys():
                if key[1] == y and key[0] == x:
                    normal_term += np.exp(self.v[0][feature[key]])
        return np.exp((linear_term - normal_term))

    def conf_mat(self):
        pass
